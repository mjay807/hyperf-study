<?php
/**
 * Created by PhpStorm
 * User: mjay
 * Date: 2023/9/26
 * Time: 15:51
 * Brief:
 * docs:
 */

declare(strict_types=1);

namespace App\Controller\Admin;


use App\Param\LoginIndexParam;
use App\Request\LoginIndexRequest;
use App\Service\LoginService;
use Hyperf\Di\Annotation\Inject;
use Mjay\HyperfHelper\Lib\AbstractController;

class LoginController extends AbstractController
{

    /**
     * @var LoginService
     */
    #[Inject]
    protected LoginService $service;

    /**
     * @param LoginIndexRequest $request
     *
     * @return array
     * 登录
     */
    public function index(LoginIndexRequest $request): array
    {
        $param = new LoginIndexParam();
        $param->username = $request->post('username', '');
        $param->password = $request->post('password', '');
        return $this->response->success($this->service->index($param));
    }

}