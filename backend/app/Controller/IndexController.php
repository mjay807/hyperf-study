<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Controller;

use Mjay\HyperfHelper\Lib\AbstractController;

class IndexController extends AbstractController
{
    public function index(): array
    {
        $user   = $this->request->input('user', 'Hyperf');
        $method = $this->request->getMethod();

        return $this->response->success(
            [
                'method'  => $method,
                'message' => "Hello {$user}.",
            ]
        );
    }
}
