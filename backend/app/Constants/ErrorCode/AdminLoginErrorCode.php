<?php
/**
 * Created by PhpStorm
 * User: mjay
 * Date: 2023/9/26
 * Time: 22:55
 * Brief:
 * docs:
 */

declare(strict_types=1);

namespace App\Constants\ErrorCode;

use Hyperf\Constants\Annotation\Constants;
use Mjay\HyperfHelper\Constants\BaseCode;

#[Constants]
class AdminLoginErrorCode extends BaseCode
{

    /**
     * @Message("没有找到员工")
     */
    const ADMIN_NOT_FUND = 10001;

    /**
     * @Message("密码错误")
     */
    const PASSWORD_ERROR = 10002;
}