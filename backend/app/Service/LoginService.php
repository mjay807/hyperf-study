<?php
/**
 * Created by PhpStorm
 * User: mjay
 * Date: 2023/9/26
 * Time: 22:31
 * Brief:
 * docs:
 */

declare(strict_types=1);

namespace App\Service;

use App\Constants\ErrorCode\AdminLoginErrorCode;
use App\Data\AdminData;
use App\Model\AdminModel;
use App\Param\LoginIndexParam;
use Hyperf\Di\Annotation\Inject;
use HyperfExtension\Hashing\Hash;
use Mjay\HyperfHelper\Exception\ServiceException;
use Mjay\HyperfHelper\Lib\Basic\BaseService;

class LoginService extends BaseService
{

    /**
     * @var AdminData
     */
    #[Inject]
    protected AdminData $adminData;

    /**
     * @param LoginIndexParam $param
     *
     * @return array
     * 登录
     */
    public function index(LoginIndexParam $param): array
    {
        $admin = $this->adminData->getUserByLoginNameOrTelephone($param->username);

        if (empty($admin))
            throw new ServiceException(AdminLoginErrorCode::ADMIN_NOT_FUND);
        if (!Hash::check($param->password . $admin['salt'], $admin['password']))
            throw new ServiceException(AdminLoginErrorCode::PASSWORD_ERROR);
        $admin['token'] = auth()->login(AdminModel::first($admin['id']));
        return $admin;
    }

}