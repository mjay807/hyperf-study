<?php
/**
 * @author XJ.
 * Date: 2022/8/29 0029
 */

namespace App\Aspect;

use Hyperf\Amqp\Message\ConsumerMessage;
use Hyperf\Command\Annotation\Command;
use Hyperf\Contract\ContainerInterface;
use Hyperf\Crontab\Annotation\Crontab;
use Hyperf\Crontab\Strategy\Executor;
use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\HttpServer\Server as HttpServer;
use Hyperf\Logger\LoggerFactory;
use Hyperf\Process\AbstractProcess;
use Hyperf\Tracer\SpanStarter;
use OpenTracing\Tracer;
use Psr\Log\LoggerInterface;
use App\Common\RequestLifeType;
use App\Common\TraceEntity;

#[Aspect]
class AddLiftTypeAspect extends AbstractAspect
{
    use SpanStarter;

    protected Tracer $tracer;


    protected LoggerInterface  $loggerFactory;

    private ContainerInterface $container;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->tracer    = $container->get(Tracer::class);
        $this->loggerFactory = make(LoggerFactory::class)->get('log', 'default');

    }

    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        if (isset($proceedingJoinPoint->getAnnotationMetadata()->class[Crontab::class])) {
            /** @var Crontab $crontab */
            $crontab = $proceedingJoinPoint->getAnnotationMetadata()->class[Crontab::class];
            if ($crontab->callback[1] === $proceedingJoinPoint->methodName) {
                // 定时任务
                RequestLifeType::setLifeType(RequestLifeType::$isCron);
                $span = $this->startSpan('crontab');
                //自定义跟踪链路,放入协程全局容器
                TraceEntity::setInstance($span->getContext()->getContext()->getTraceId(), 'crontab:'.$crontab->name, env('APP_NAME'));
            }
        }

        if (isset($proceedingJoinPoint->getAnnotationMetadata()->class[Command::class])){
            if ($proceedingJoinPoint->methodName === 'handle'){
                RequestLifeType::setLifeType(RequestLifeType::$isCron);
                /** @var \Hyperf\Command\Command $instance */
                $instance = $proceedingJoinPoint->getInstance();
                $span     = $this->startSpan('cmd');
                // 自定义跟踪链路,放入协程全局容器
                TraceEntity::setInstance($span->getContext()->getContext()->getTraceId(), 'cmd:'.$instance->getName(), env('APP_NAME'));
            }
        }

        switch (true) {
            case $proceedingJoinPoint->className === HttpServer::class && $proceedingJoinPoint->methodName === 'onRequest':
                // rest
                // HttpTraceMiddleware中间件已加载
                RequestLifeType::setLifeType(RequestLifeType::$isRest);
                break;
            case $proceedingJoinPoint->className === AbstractProcess::class && $proceedingJoinPoint->methodName === 'bindServer':
                // 进程
                RequestLifeType::setLifeType(RequestLifeType::$isProcess);
                break;
//            case $proceedingJoinPoint->className === RpcServer::class && $proceedingJoinPoint->methodName === 'onReceive':
//                // rpc
//                // JsonRpcAspect 切面已加载
//                RequestLifeType::setLifeType(RequestLifeType::$isRpc);
//                break;
            case $proceedingJoinPoint->className === Executor::class && $proceedingJoinPoint->methodName === 'execute':
                RequestLifeType::setLifeType(RequestLifeType::$isCron);
                break;
            case $proceedingJoinPoint->className === ConsumerMessage::class && $proceedingJoinPoint->methodName === 'consumeMessage':
                // mq消费者
                // ListenAmqpConsumerExecuteAspect 切面已加载
                RequestLifeType::setLifeType(RequestLifeType::$isMQ);
                $span = $this->startSpan('mq');
                // 自定义跟踪链路,放入协程全局容器
                TraceEntity::setInstance($span->getContext()->getContext()->getTraceId(), 'mq', env('APP_NAME'));
                break;
        }

        $res = $proceedingJoinPoint->process();

        if (isset($span)) {
            $span->finish();
        }

        return $res;

    }
}