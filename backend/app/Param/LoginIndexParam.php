<?php
/**
 * Created by PhpStorm
 * User: mjay
 * Date: 2023/9/26
 * Time: 22:27
 * Brief:
 * docs:
 */

declare(strict_types=1);

namespace App\Param;

class LoginIndexParam
{

    /**
     * @var string 账号
     */
    public string $username = '';

    /**
     * @var string 密码
     */
    public string $password = '';

}