<?php

declare (strict_types=1);
namespace App\Model;

use Hyperf\Utils\HigherOrderTapProxy;
use HyperfExtension\Auth\Authenticatable;
use HyperfExtension\Auth\Contracts\AuthenticatableInterface;
use HyperfExtension\Jwt\Contracts\JwtSubjectInterface;
use Mjay\HyperfHelper\Lib\Basic\BaseModel;

/**
 * @property int $id ID
 * @property int $created_time 创建时间
 * @property int $created_user 创建人
 * @property int $updated_time 更新时间
 * @property int $updated_user 更新人
 * @property int $deleted_time 删除时间
 * @property int $deleted_user 删除人
 * @property string $login_user_name 登录用户名
 * @property string $username 用户名
 * @property string $nickname 昵称
 * @property string $real_name 真实姓名
 * @property string $telephone 电话号码
 * @property string $password 密码
 * @property string $salt 密码盐
 * @property string $avatar 头像
 * @property string $email 邮箱
 * @property string $id_number 编号
 * @property int $gender 性别 1-男 2-女
 * @property string $status 状态 1-正常 2-禁用
 * @mixin \App_Model_Admin
 */
class AdminModel extends BaseModel implements JwtSubjectInterface, AuthenticatableInterface
{
    //增加 auth 支持
    use Authenticatable;
    /**
     * The table associated with the model.
     *
     * @var ?string
     */
    protected ?string $table = 'admin';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected array $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected array $casts = ['id' => 'integer', 'created_time' => 'integer', 'created_user' => 'integer', 'updated_time' => 'integer', 'updated_user' => 'integer', 'deleted_time' => 'integer', 'deleted_user' => 'integer', 'gender' => 'integer'];

    /**
     * @return HigherOrderTapProxy|mixed|null
     */
    public function getJwtIdentifier()
    {
        // TODO: Implement getJwtIdentifier() method.
        return $this->getKey();
    }

    /**
     * @return string[]
     */
    public function getJwtCustomClaims(): array
    {
        // TODO: Implement getJwtCustomClaims() method.
        // 添加自定义信息
        return [
            'guard' => 'admin'
        ];
    }
}