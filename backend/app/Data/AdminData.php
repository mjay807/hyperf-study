<?php
/**
 * Created by PhpStorm
 * User: mjay
 * Date: 2023/9/26
 * Time: 22:51
 * Brief:
 * docs:
 */

declare(strict_types=1);

namespace App\Data;

use App\Model\AdminModel;
use Mjay\HyperfHelper\Lib\Basic\BaseData;

class AdminData extends BaseData
{

    /**
     * @param string $username
     *
     * @return array|null
     * 读取用户
     */
    public function getUserByLoginNameOrTelephone(string $username): ?array
    {
        return AdminModel::query()->where('login_name', $username)
            ->orWhere('telephone', $username)
            ->first()?->toArray();
    }


}