<?php

declare(strict_types=1);

namespace App\Middleware;

use Exception;
use Hyperf\Di\Annotation\Inject;
use HyperfExtension\Jwt\Exceptions\TokenExpiredException;
use HyperfExtension\Jwt\JwtFactory;
use Mjay\HyperfHelper\Constants\BaseCode;
use Mjay\HyperfHelper\Exception\CodeException;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class JwtMiddleware implements MiddlewareInterface
{
    /**
     * @var ContainerInterface
     */
    protected ContainerInterface $container;

    /**
     * 注入 JwtFactory
     * @var JwtFactory
     */
    #[Inject]
    protected JwtFactory $jwtFactory;

    /**
     * @var ResponseInterface
     */
    protected ResponseInterface $response;


    public function __construct(ContainerInterface $container, ResponseInterface $response)
    {
        $this->container = $container;
        $this->response = $response;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $jwt = $this->jwtFactory->make();

        try {
            $jwt->checkOrFail();
        } catch (Exception $exception) {
            if (! $exception instanceof TokenExpiredException) {
                throw new CodeException(BaseCode::API_NOT_LOGIN, $exception->getMessage());
            }
            //  尝试自动刷新 token
            try {
                $token = $jwt->getToken();

                // 刷新token
                $new_token = $jwt->getManager()->refresh($token);

                // 解析token载荷信息
                $payload = $jwt->getManager()->decode($token, false, true);

                // 旧token加入黑名单
                $jwt->getManager()->getBlacklist()->add($payload);

                // 一次性登录，保证此次请求畅通
                auth($payload->get('guard') ?? config('auth.default.guard'))
                    ->onceUsingId($payload->get('sub'));

                return $handler->handle($request)->withHeader('authorization', 'bearer ' . $new_token);
            } catch (Exception $exception) {
                //    Token 验证失败
                throw new CodeException(BaseCode::API_NOT_LOGIN, $exception->getMessage());
            }
        }

        return $handler->handle($request);
    }
}