<?php


namespace App\Middleware;


use Hyperf\Tracer\Middleware\TraceMiddleware;
use Hyperf\Utils\Coroutine;
use Mjay\HyperfHelper\Common\RequestLifeType;
use Mjay\HyperfHelper\Common\TraceEntity;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use OpenTracing\Span;

/**
 * http服务链路跟踪加强
 * Class HttpMiddleware
 * @package Scjc\Lib\Middleware
 */
class HttpTraceMiddleware extends TraceMiddleware
{
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($request->getUri()->getPath() !== '/favicon.ico') {
            RequestLifeType::setLifeType(RequestLifeType::$isRest);
            $res = parent::process($request, $handler);
            TraceEntity::clearData();
            return $res;
        }
        return $handler->handle($request);
    }

    protected function buildSpan(ServerRequestInterface $request): Span
    {
        $uri = $request->getUri();
        $span = $this->startSpan('request');
        //自定义跟踪链路,放入协程全局容器
        TraceEntity::setInstance($span->getContext()->getContext()->getTraceId(), $request->getUri()->getPath(), env('APP_NAME'));

        $span->setTag('coroutine.id', (string) Coroutine::id());
        $span->setTag('request.path', (string) $uri);
        $span->setTag('request.method', $request->getMethod());
        foreach ($request->getHeaders() as $key => $value) {
            $span->setTag('request.header.' . $key, implode(', ', $value));
        }
        return $span;
    }
}