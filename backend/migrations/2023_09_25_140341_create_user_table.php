<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Schema\Schema;
use Mjay\HyperfHelper\Constants\Lib\MigrationConstants;
use Mjay\HyperfHelper\Lib\MigrationAbstract;

class CreateUserTable extends MigrationAbstract
{
    protected string $tableName = 'admin';

    /**
     * @throws Exception
     */
    public function exec(Blueprint $table): void
    {
        if (! Schema::hasColumn($this->tableName, 'login_user_name')) {
            $table->string('login_user_name', 50)->default('')->comment('登录用户名');
        }
        if (! Schema::hasColumn($this->tableName, 'username')) {
            $table->string('username', 50)->default('')->comment('用户名');
        }
        if (! Schema::hasColumn($this->tableName, 'nickname')) {
            $table->string('nickname', 50)->default('')->comment('昵称');
        }
        if (! Schema::hasColumn($this->tableName, 'real_name')) {
            $table->string('real_name', 50)->default('')->comment('真实姓名');
        }
        if (! Schema::hasColumn($this->tableName, 'telephone')) {
            $table->string('telephone', 50)->default('')->comment('电话号码');
        }
        if (! Schema::hasColumn($this->tableName, 'password')) {
            $table->string('password', 255)->default('')->comment('密码');
        }
        if (! Schema::hasColumn($this->tableName, 'salt')) {
            $table->string('salt', 255)->default('')->comment('密码盐');
        }
        if (! Schema::hasColumn($this->tableName, 'avatar')) {
            $table->string('avatar', 50)->default('')->comment('头像');
        }
        if (! Schema::hasColumn($this->tableName, 'email')) {
            $table->string('email', 50)->default('')->comment('邮箱');
        }
        if (! Schema::hasColumn($this->tableName, 'id_number')) {
            $table->string('id_number', 50)->default('')->comment('编号');
        }
        if (! Schema::hasColumn($this->tableName, 'gender')) {
            $table->tinyInteger('gender')->default(1)->comment('性别 1-男 2-女');
        }
        if (! Schema::hasColumn($this->tableName, 'status')) {
            $table->string('status', 50)->default(1)->comment('状态 1-正常 2-禁用');
        }

        $this->setIndex($table, MigrationConstants::INDEX, ['login_user_name']);
        $this->setIndex($table, MigrationConstants::INDEX, ['username']);
        $this->setIndex($table, MigrationConstants::INDEX, ['nickname']);
        $this->setIndex($table, MigrationConstants::INDEX, ['telephone']);
        $this->setIndex($table, MigrationConstants::INDEX, ['gender']);
        $this->setIndex($table, MigrationConstants::INDEX, ['status']);
    }
}
