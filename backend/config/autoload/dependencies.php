<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */


use Mjay\HyperfHelper\Lib\ResponseInterface;

return [

    ResponseInterface::class => \Mjay\HyperfHelper\Common\HttpResponse::class,

];
