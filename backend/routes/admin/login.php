<?php
/**
 * Created by PhpStorm
 * User: mjay
 * Date: 2023/9/26
 * Time: 15:49
 * Brief:
 * docs:
 */

declare(strict_types=1);

use App\Controller\Admin\LoginController;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/login', function () {
    // 登录
    Router::post('/index', [LoginController::class, 'index']);
});