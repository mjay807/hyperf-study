<?php
/**
 * Created by PhpStorm
 * User: mjay
 * Date: 2023/9/26
 * Time: 15:49
 * Brief:
 * docs:
 */

declare(strict_types=1);


use Hyperf\HttpServer\Router\Router;

Router::addGroup('/admin', function () {
    include __DIR__ . '/admin/login.php';
});