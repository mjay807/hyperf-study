<?php

declare(strict_types=1);

use App\Model\AdminModel;
use Hyperf\Database\Seeders\Seeder;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Snowflake\IdGenerator;
use HyperfExt\Hashing\Hash;

class CreateUserSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $salt = md5((string) rand(10000, 999999));
        //
        AdminModel::insert(
            [
                'login_user_name' => 'admin',
                'username' => 'admin',
                'nickname' => 'admin',
                'real_name' => 'admin',
                'telephone' => 'admin',
                'password' => Hash::make('111111' . $salt),
                'salt' => $salt,
                'id_number' => 'admin',
                'created_time' => time(),
            ]
        );
    }
}
